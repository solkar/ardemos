﻿//
//
// Bear  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;
using Vuforia;

[RequireComponent ( typeof ( TrackableBehaviour ) ) ]
public class Bear : MonoBehaviour , ITrackableEventHandler
{
    #region Private Variables

    [SerializeField] ParticleSystem spawn;
    
    #endregion

#region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {
        var mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

#endregion // UNTIY_MONOBEHAVIOUR_METHODS

#region ITrackableEventHandler 

    public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus,
            TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

#endregion // PUBLIC_METHODS



#region PRIVATE_METHODS


    private void OnTrackingFound()
    {
        spawn.Play();
    }


    private void OnTrackingLost()
    {
    }

#endregion

}
