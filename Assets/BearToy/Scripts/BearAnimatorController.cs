﻿using UnityEngine;
using System.Collections;

public class BearAnimatorController : MonoBehaviour 
{

    NavMeshAgent nav;
    Animator animator;
    
	void Start () 
    {
        nav = GetComponent <NavMeshAgent> ();
        animator = GetComponent<Animator>();
	}
	
	void Update () 
    {
        var vel = nav.velocity.magnitude;
        if( vel > 0 )
        {
            animator.SetBool( "isMoving" , true );
        }
        else
        {
            animator.SetBool( "isMoving" , false );
        }

	

	}
}
