﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{
    Transform player;
    NavMeshAgent nav;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        nav = GetComponent <NavMeshAgent> ();
    }


    void Update ()
    {
        if( player.gameObject.activeSelf )
        {
            nav.enabled = true;
            nav.SetDestination (player.position);

            Debug.DrawRay( transform.position , player.position , Color.green );
        }
        else
        {
            nav.enabled = false;
        }
    }
}
