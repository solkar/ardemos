﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DonutHill.UI;

public class ContextButtonHandler : MonoBehaviour
{

    [SerializeField] ContextButton button;

    bool contextActive = false;

    void Update()
    {
        if( contextActive )
        {
            button.gameObject.SetActive( true );
        }
        else
        {
            button.gameObject.SetActive( false );
        }

        contextActive = false;
    }

    public void SwitchOn()
    {
        contextActive = true;
    }
}
