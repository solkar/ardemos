﻿using UnityEngine;
using System.Collections;

public class CoordinateDisplay : MonoBehaviour 
{

    public Transform target;

    TextMesh text;

	void Start () 
    {
        text = GetComponent<TextMesh>();

	}
	
	void Update () 
    {
        float coordinate = target.position.y;
        text.text = coordinate.ToString( "F" );
	
	}
}
