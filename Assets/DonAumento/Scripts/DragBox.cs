using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DonutHill.UI;
using Vectrosity;

namespace DonutHill.DonAumento
{
    public class DragBox : MonoBehaviour
    {
        #region Public Variables

        public ContextButton button;
        
        #endregion

        #region Constants

        const float k_Spring               = 500.0f;
        const float k_Damper               = 5.0f;
        const float k_Drag                 = 10.0f;
        const float k_AngularDrag          = 5.0f;
        //const float k_Distance             = 0.2f;
        [SerializeField] float k_Distance             = 0.2f;
        const bool  k_AttachToCenterOfMass = true;

        #endregion

        #region Private Variables 

        private SpringJoint m_SpringJoint;
        private bool m_Active;

        #endregion

        #region MonoBehaviour Functions

        void Start()
        {
            button.onPress.AddListener( OnDragHold );
            button.onRelease.AddListener( OnDragRelease );
        }

        private void Update()
        {
            if ( !m_Active )
            {
                return;
            }

            var mainCamera = FindCamera();

            // We need to actually hit an object
            RaycastHit hit = new RaycastHit();
            if (
                !Physics.Raycast(mainCamera.transform.position,
                                 mainCamera.transform.forward, 
                                 out hit, 
                                 100,
                                 Physics.DefaultRaycastLayers))
            {
                return;
            }
            // We need to hit a rigidbody that is not kinematic
            if (!hit.rigidbody || hit.rigidbody.isKinematic)
            {
                return;
            }

            if (!m_SpringJoint)
            {
                var go = new GameObject("Rigidbody dragger");
                Rigidbody body = go.AddComponent<Rigidbody>();
                m_SpringJoint = go.AddComponent<SpringJoint>();
                body.isKinematic = true;
            }

            // Input 1: Hit.point
            if ( k_AttachToCenterOfMass )
            {
                var anchor = transform.TransformDirection(hit.rigidbody.centerOfMass) + hit.rigidbody.transform.position;
                anchor = m_SpringJoint.transform.InverseTransformPoint(anchor);
                m_SpringJoint.anchor = anchor;
            }
            else
            {
                m_SpringJoint.anchor = Vector3.zero;
            }

            m_SpringJoint.transform.position = hit.point;
            m_SpringJoint.spring             = k_Spring;
            m_SpringJoint.damper             = k_Damper;
            m_SpringJoint.maxDistance        = k_Distance;
            m_SpringJoint.connectedBody      = hit.rigidbody;

            StartCoroutine("DragObject", hit.distance);
        }

#endregion

        void OnDragHold()
        {
            m_Active = true;
        }

        void OnDragRelease()
        {
            m_Active = false;
        }

        private IEnumerator DragObject(float distance)
        {
            var oldDrag        = m_SpringJoint.connectedBody.drag;
            var oldAngularDrag = m_SpringJoint.connectedBody.angularDrag;

            m_SpringJoint.connectedBody.drag        = k_Drag;
            m_SpringJoint.connectedBody.angularDrag = k_AngularDrag;

            var linePoints = new List<Vector3>();
            linePoints.Add( transform.position );
            linePoints.Add( transform.position );
            var line = new VectorLine( "Joint" , linePoints , 2.0f );

            var mainCamera = FindCamera();
            while ( m_Active )
            {
                var ray = mainCamera.ScreenPointToRay( 
                        new Vector2( mainCamera.pixelWidth * 0.5f , 
                                     mainCamera.pixelHeight * 0.5f ) );
                m_SpringJoint.transform.position = ray.GetPoint(distance);

                linePoints[ 0 ] = ray.GetPoint( distance );
                linePoints[ 1 ] = transform.position;

                line.Draw();

                yield return null;
            }

            VectorLine.Destroy( ref line );

            if (m_SpringJoint.connectedBody)
            {
                m_SpringJoint.connectedBody.drag = oldDrag;
                m_SpringJoint.connectedBody.angularDrag = oldAngularDrag;
                m_SpringJoint.connectedBody = null;
            }
        }


        private Camera FindCamera()
        {
            if (GetComponent<Camera>())
            {
                return GetComponent<Camera>();
            }

            return Camera.main;
        }
    }
}
