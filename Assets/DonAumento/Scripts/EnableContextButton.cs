﻿using UnityEngine;
using System.Collections;
using DonutHill.UI;

public class EnableContextButton : MonoBehaviour 
{

	
	void Update () 
    {
        Debug.DrawRay( transform.position , transform.forward , Color.green );	

		RaycastHit hit;
		if( Physics.Raycast( transform.position , transform.forward , out hit ) )
		{
			// TODO: detect context button
			if( hit.collider.CompareTag( "ContextButton" ) )
			{
                var contextAction = hit.collider.GetComponent<ContextButtonHandler>();
                contextAction.SwitchOn();
			}
		}
	}
}
