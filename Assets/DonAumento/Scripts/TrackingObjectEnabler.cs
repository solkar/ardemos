﻿using UnityEngine;
using System.Collections;


namespace DonutHill.DonAumento
{
    public class TrackingObjectEnabler : MonoBehaviour 
    {
        public GameObject target;

        void OnTrackFound()
        {
            target.SetActive( true );
        }

        void OnTrackLost()
        {
            target.SetActive( false );
        }

    }
}
