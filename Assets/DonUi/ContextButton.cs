﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DonutHill.UI
{
    public class ContextButton : Selectable, 
                                 IPointerDownHandler, 
                                 IPointerUpHandler
                                 // IPointerEnterHandler,
                                 // IPointerExitHandler
    {
        [Serializable]
        public class ButtonClickedEvent : UnityEvent {}

        // Event delegates triggered on click.
        [SerializeField]
        private ButtonClickedEvent m_OnPress = new ButtonClickedEvent();
        [SerializeField]
        private ButtonClickedEvent m_OnRelease = new ButtonClickedEvent();

        protected ContextButton()
        {}

        public ButtonClickedEvent onPress
        {
            get { return m_OnPress; }
            set { m_OnPress = value; }
        }

        public ButtonClickedEvent onRelease
        {
            get { return m_OnRelease; }
            set { m_OnRelease = value; }
        }


        void OnDisable()
        {
            Release();    
            m_OnRelease.Invoke();
        }

        #region Selectable Event Handlers

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            Press();
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            Release();
        }

        #endregion

        private void Press()
        {
            if (!IsActive() || !IsInteractable())
                return;

            m_OnPress.Invoke();
        }

        private void Release()
        {
            if (!IsActive() || !IsInteractable())
                return;

            m_OnRelease.Invoke();
        }

    }
}

