﻿//
//
// DetectFace  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class DetectFace : MonoBehaviour 
{
    public DotMarker marker;

    #region Collisions

    void OnTriggerEnter( Collider col )
    {
        if( col.CompareTag( "Green" ) )
        {
            marker.SetMarker( DotMarker.Marker.Green );
        }
        else if( col.CompareTag( "Red" ) )
        {
            marker.SetMarker( DotMarker.Marker.Red );
        }
    }

    void OnTriggerExit( Collider col )
    {
        if( col.CompareTag( "Green" )
                || col.CompareTag( "Red" ) )
        {
            marker.SetMarker( DotMarker.Marker.Empty );
        }
    }

    #endregion

}
