﻿//
//
// DotMarker  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

[RequireComponent ( typeof ( SpriteRenderer ) ) ]   
public class DotMarker : MonoBehaviour 
{
    public enum Marker
    {
        Empty,
        Blue,
        Green,
        Grey,
        Red,
        Yellow
    }

    [SerializeField] Sprite[] markerSpriteArray;

    Marker selectedMarker;
    SpriteRenderer _spriteRenderer;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        SetMarker( Marker.Empty );
    }

    public void SetMarker( Marker selected )
    {
        selectedMarker = selected;

        _spriteRenderer.sprite = markerSpriteArray[ (int) selectedMarker ];

    }

}
