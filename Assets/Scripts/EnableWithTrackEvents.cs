﻿//
//
// EnableWithTrackEvents  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class EnableWithTrackEvents : MonoBehaviour 
{
    [SerializeField] GameObject target;

    public void OnTrackFound()
    {
        target.SetActive( true );
    }

    public void OnTrackLost()
    {
        target.SetActive( false );
    }
        

}
