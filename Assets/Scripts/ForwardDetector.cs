﻿//
//
// ForwardDetector  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class ForwardDetector : MonoBehaviour 
{

    [SerializeField] float radius      = 0.5f;
    [SerializeField] float maxDistance = 5f;

    void FixedUpdate()
    {

        RaycastHit hit;

        float distanceToObstacle = 0;
        
        if (Physics.SphereCast( transform.position , radius , transform.forward, out hit , maxDistance )) 
        {
        	distanceToObstacle = hit.distance;

            Debug.DrawRay( transform.position , transform.forward * distanceToObstacle , Color.green );
        }
        else
        {
            Debug.DrawRay( transform.position , transform.forward * maxDistance , Color.grey );
        }
    }

}
