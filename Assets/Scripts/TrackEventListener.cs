﻿//
//
// TrackEventListener  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;
using Vuforia;

[RequireComponent ( typeof ( TrackableBehaviour ) ) ]
public class TrackEventListener : MonoBehaviour , ITrackableEventHandler
{

#region MonoBehaviour Functions

    void Start()
    {
        var mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

    }

#endregion // UNTIY_MONOBEHAVIOUR_METHODS

#region ITrackableEventHandler 

    public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus,
            TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            SendMessage( "OnTrackFound" , SendMessageOptions.DontRequireReceiver ); 
        }
        else
        {
            SendMessage( "OnTrackLost" , SendMessageOptions.DontRequireReceiver ); 
        }
        
    }

#endregion
}
