﻿//
//
// TrackableSoundCues  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;
using Vuforia;

[RequireComponent ( typeof ( TrackableBehaviour ) ) ]
[RequireComponent ( typeof ( AudioSource ) ) ]
public class TrackableSoundCues : MonoBehaviour , ITrackableEventHandler
{
#region Private Variables

    [SerializeField] AudioClip found;
    [SerializeField] AudioClip lost;

    AudioSource audio;

#endregion

#region UNTIY_MONOBEHAVIOUR_METHODS

    void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    void Start()
    {
        var mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

    }

#endregion // UNTIY_MONOBEHAVIOUR_METHODS

#region ITrackableEventHandler 

    public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus,
            TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            
            audio.clip = found;
            if( audio.clip != null )
                audio.Play();
        }
        else
        {
            audio.clip = lost;
            if( audio.clip != null )
                audio.Play();
        }
        
    }

#endregion

}
