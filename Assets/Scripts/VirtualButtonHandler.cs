﻿//
//
// VirtualButtonHandler  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;
using Vuforia;

public class VirtualButtonHandler : MonoBehaviour , IVirtualButtonEventHandler
{
    #region Public variables

    public VirtualButtonBehaviour button;

    #endregion

    #region MonoBehaviour Functions

    void Start()
    {
        if ( button )
        {
            button.RegisterEventHandler(this);
        }
    }

    #endregion

    #region IVirtualButtonEventHandler

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        Debug.Log("OnButtonPressed");
        GetComponent<AudioSource>().Play();
    }


    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {
        Debug.Log("OnButtonReleased");
    }

    #endregion
}
