# README #

Technical demo using testing capabilities of Vuforia Augmented Reality library

demo: https://vine.co/v/ell229wn6Am

### Summary ###

Using target cards an animated character is loaded into the screen.
Another target card is used to trigger follow behaviour in the character.